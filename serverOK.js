const express = require('express'); //identifico las despendencias
const app = express ();              //inicializo, en cada dependencia tengo que mirar la documentacion

app.use(express.json()); // indicas que el procesamiento es como un json

const port = process.env.PORT || 3000 //puerto donde corre mi aplicacion

app.listen(port);
console.log("API escuchando en el puerto BIP BIP BIP" + port);

app.get("/apitechu/v1/hello",
  function(req,res){ // request y response
    console.log("GET /apitechu/v1/hello");

    //res.send('{"msg" : "Hola desde APITechu"}'); // indico la respuesta a la petición como un string
    res.send({"msg" : "Hola desde APITechu"}); // indico la respuesta a la petición como un JSON. tenemos que tener {}
  }
) //voy a registrar una ruta indicando el recurso al que llamo

app.get("/apitechu/v1/users",
  function(req,res){
    console.log("GET /apitechu/v1/users");

  //  res.sendFile('usuarios.json', {root: __dirname});
  var users =require('./usuarios.json');
  res.send(users);
  }
) //leer usuarios de un fichero

app.post("/apitechu/v1/users",
  function(req,res){
    console.log("POST /apitechu/v1/users");

  //  console.log(req.body);  me muestra la informacion del body    console.log(req.body.first_name);
    console.log(req.body.last_name);
    console.log(req.body.email);

    var newUser = {
      "first_name" : req.body.first_name,
      "last_name" : req.body.last_name,
      "email" : req.body.email
    };

    var users = require ('./usuarios.json');
    users.push(newUser); //meto un elemento al alert-primary
    console.log("usuario añadido al array");

    const fs =require('fs');  //preparo lo que necesito
    var jsonUserData = JSON.stringify(users);

    fs.writeFile("./usuarios.json", jsonUserData, "utf8",
       function (err){
         if (err){
         console.log(err);
         } else{
         console.log("Usuario escrito en fichero");
         }
      }
  )
  }
) //crear un usuario

//mosntruo/:  a partir de los dos puntos le indico los parametros
app.post("/apitechu/v1/monstruo/:p1/:p2",
  function(req,res){
      console.log("GET /apitechu/v1/monstruo/:v1/:v2");

console.log("Parametros:");
console.log(req.params);

console.log("Query String");
console.log(req.query);

console.log("Headers");
console.log(req.headers);
console.log("Body");
console.log(req.body);

  }
)  //prueba de un post
