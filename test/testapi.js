const mocha = require ('mocha');
const chai = require ('chai');
const chaihttp = require ('chai-http');

chai.use(chaihttp);

var should = chai.should(); // se usura para las aserciones

describe("First test",     // con describe defines una suite de test
  function (){
    it ("Test that DuckDuckGo works",function (done){ // cada it es un test unitario , done es la funcion manejadora,
      chai.request("http://www.duckduckgo.com")
         .get('/') //lo que va despues del dominio, a donde voy
         .end(
           function (err, res){                               //función manejadora
             console.log("Request finished");
      //       console.log(res);                   //respuesta del test
      //       console.log(err);                   // error del test
             res.should.have.status (200);          //primera aserción, error 200 es respuesta OK
             done();
           }
         )
    }
   )
  }
)

// tengo que hacer comprobaciones de lo que espero. pero para ver si el test esta bien tienes que probar a cambiar lo experado y que de + expected -actual
describe("Test Api Usuarios",     // con describe defines una suite de test
  function (){
    it ("Prueba que la Api responde",function (done){ // cada it es un test unitario , done es la funcion manejadora,
      chai.request("http://localhost:3000")
         .get('/apitechu/v1/hello') //lo que va despues del dominio, a donde voy
         .end(
           function (err, res){                               //función manejadora
             console.log("Request finished");
             res.should.have.status (200);
             res.body.msg.should.be.eql("Hola desde APITechu");  //eql = es igual, a partir del should son las comprobaciones
             done();
           }
         )
    }
  ),  //concateno varios it
  it ("Prueba que la Api devuelve una lista de usuarios correctos",function (done){ // espero una lista
    chai.request("http://localhost:3000")
       .get('/apitechu/v1/users') //lo que va despues del dominio, a donde voy
       .end(
         function (err, res){                               //función manejadora
           console.log("Request finished");
           res.body.should.be.a("array");
          //  for (user of res.body.users){
          //    user.should.have.property('first_name');
          //    user.should.have.property('last_name');
          //  }
           for (user of res.body){    //user se va cargando en cada iteracion con los datos de la iteracion
                 user.should.have.property('first_name');
                user.should.have.property('last_name');
           }
           done();
         }
       )
  }
  )
 }
)
