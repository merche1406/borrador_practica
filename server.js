require ('dotenv').config();  // carga variable de entorno
const express = require('express'); //identifico las despendencias
const app = express ();              //inicializo, en cada dependencia tengo que mirar la documentacion

const userController = require('./controllers/UserController');
const authController = require ('./controllers/AuthController');
const accountController = require ('./controllers/AccountController');
const movController = require ('./controllers/MovController');

app.use(express.json()); // indicas que el procesamiento es como un json

const port = process.env.PORT || 3000 //puerto donde corre mi aplicacion

var enableCORS =function(req,res,next){
  res.set("Access-Control-Allow-Origin", "*");
  res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
  res.set("Access-Control-Allow-Headers", "Content-Type");
  next();
}
app.use(enableCORS);

app.listen(port);
console.log("API escuchando en el puerto BIP BIP BIP" + port);

app.get("/apitechu/v1/hello",
  function(req,res){ // request y response
    console.log("GET /apitechu/v1/hello");
    res.send({"msg" : "Hola desde APITechu"}); // indico la respuesta a la petición como un JSON. tenemos que tener {}
  }
) //voy a registrar una ruta indicando el recurso al que llamo

app.get("/apitechu/v1/users",
function(req,res){
  console.log("GET /apitechu/v1/users");

//  res.sendFile('usuarios.json', {root: __dirname});
var users =require('./usuarios.json');
res.send(users);
}
) //leer usuarios de un fichero

//app.get("/apitechu/v1/users", userController.getUsersV1);
app.get("/apitechu/v2/users", userController.getUsersV2);
app.get("/apitechu/v2/users/:id", userController.getUsersByIdV2);
//app.post("/apitechu/v1/users", userController.createUserV1);//crear un usuario
app.post("/apitechu/v2/users", userController.createUserV2);//crear un usuario

app.get("/apitechu/v2/busqueda/:email", userController.getUsersByEmail);
//app.delete("/apitechu/v1/users/:id", userController.deleteUserV1);
app.delete("/apitechu/v2/users/:id", userController.deleteteUserV2);
//app.post("/apitechu/v1/login", authController.loginUsersV1);
app.post("/apitechu/v2/login", authController.loginUsersV2);
app.post("/apitechu/v1/logout/:id",authController.logoutUsersV1);
app.post("/apitechu/v2/logout/:id",authController.logoutUsersV2);

app.get("/apitechu/v2/account/:id", accountController.getAccountById);
app.post("/apitechu/v2/account/:id", accountController.createAccount);
app.delete("/apitechu/v2/account/:id", accountController.deleteAccount);

app.get("/apitechu/v2/movement/:id", movController.consultarMov);
app.post("/apitechu/v2/movement", movController.altaMov);
app.post("/apitechu/v2/ingreso", movController.ingresoMov);
app.post("/apitechu/v2/reintegro", movController.reintegroMov);
app.post("/apitechu/v2/transfer", movController.transferMov);

/////delete un ussurio
// app.delete("/apitechu/v1/users/:id",
//   function(req,res){
//     console.log("DELETE /apitechu/v1/users/:id");
//
//     console.log("la id del usuario a borrar es:" + req.params.id);
//     var users = require('./usuarios.json');
//     users.splice(req.params.id -1,1);
//     writeUserDataToFile(users);
//     console.log("Proceso de borrado de usuario terminado");
//
//     res.send({"msg": "usuario borrado"});
//   }
// )

////delete un ussurio


// for (var id=0;id<users.length;id++)
//   {
//     console.log("por donde voy:" + id);
//     if (req.params.id ==id){
//       users.splice(req.params.id -1,1);
//       writeUserDataToFile(users);
//       console.log("Proceso de borrado de usuario terminado");
//
//       res.send({"msg": "usuario borrado"});
//
//     }

//mosntruo/:  a partir de los dos puntos le indico los parametros
app.post("/apitechu/v1/monstruo/:p1/:p2",
  function(req,res){
      console.log("GET /apitechu/v1/monstruo/:v1/:v2");

console.log("Parametros:");
console.log(req.params);

console.log("Query String");
console.log(req.query);

console.log("Headers");
console.log(req.headers);
console.log("Body");
console.log(req.body);

  }
)  //prueba de un post
