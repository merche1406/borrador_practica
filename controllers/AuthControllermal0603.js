const io= require('../io.js');

function loginUsersV1(req, res) {
  console.log("GET /apitechu/v1/login");

  var result = {};
  var users = require('../usuarios.json'); // .. que estoy en otra carpeta
  var encontrado = 0;

  for (var id=0;id<users.length;id++)
    {
      //  console.log("Proceso busqueda de email:" + users[id].email);
      if (req.body.email == users[id].email) {
            console.log("Email encontrado");
          );
            if (req.body.password== users[id].password){
                res.send({"msg": "Login correcto, idUsuario :"+ users[id].id});
                encontrado =1;
                break;
            }else {
              console.log("Password no encontrado:");
    //          res.send({"msg": "Login incorrecto"});
            }
      }

    }
  // if (id== users.length){
  //   console.log("Email no encontrado");
  // }
 if (encontrado ==1){
      users[id].logged =true;
      console.log("Usuario a logear:" + users[id].logged);
   io.writeUserDataToFile(users);
 }
 else {
   res.send({"msg": "Login incorrecto"});
 }
 }
 // POST a /apitechu/v1/logout/:id
  // Id es la id del usuario a deslogar.
 // Tanto para logout correcto como incorrecto, devolver un mensaje informativo.
 //
 // En caso de logout correcto, guardar el cambio en el archivo.
 //
 // En el logout tenemos que quitar el campo logged del usuario (delete user.logged)
 //
 // Ejemplo de respuestas:
 //
 // Logout incorrecto { "mensaje" : "logout incorrecto" }
 //
 // Logout correcto { "mensaje" : "logout correcto", "idUsuario" : 1 }

 function logoutUsersV1(req, res) {
   console.log("GET /apitechu/v1/logout");

   var result = {};
   var users = require('../usuarios.json'); // .. que estoy en otra carpeta
   var id = req.params.id;
  id = id -1;
//  console.log("la id del usuario a logout es:" + req.params.id);
  console.log("la id del usuario a logout es:" + users[id].id);
  console.log("la id del usuario a logout es:" + users[id].logged);
  if (users[id].logged ==true) {
      console.log("usuario logado");
      delete users[id].logged ;
      console.log("Usuario a logear:" + users[id].logged);
//      users.push(users[id].logged);
      io.writeUserDataToFile(users);
      res.send({"msg": "Logout correcto idUsuario :"+ req.params.id});

      //       writeUserDataToFile(users);
    }  else{
      console.log("usuario no logado");
      res.send({"msg": "Logout incorrecto"});
    }
 }

module.exports.loginUsersV1 = loginUsersV1 ;
module.exports.logoutUsersV1 = logoutUsersV1 ;
