const requestJson = require('request-json');
const io= require('../io.js');
const crypt =require ('../crypt.js');

const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechumgf11ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;


function consultarMov(req, res) {
  console.log("GET /apitechu/v2/movement/:id");

  var id = req.params.id;
//  var query = 'q={"email": "' + req.params.email + '"}';
  var query = 'q={"IBAN": "'+id + '"}';
  console.log("La consulta es:" + query);

  var httpClient = requestJson.createClient(baseMLabURL);  //inicializo mi url de conexión a la BD
  console.log("Busqueda de movimientos");
  httpClient.get("movement?"+ query + "&"+ mLabAPIKey,    //aqui le indico la colección y la apikey
   function(err,resMLab, body){          //para asegurar lo que estoy preguntando puedo hacer un console del resMLab
      if (err){
         var response = {
            "msg" : "Error obteniendo movimientos"
       }
      res.status(500);
      }else{
        if (body.length > 0){
   //           var response = body[0];
            var response = body;
        }else{
          var response = {
          "msg" : "Cuenta no encontrada"
        }
        res.status(404);
      }
   }
  res.send(response);
  }

  )

 }

 function altaMov(req, res) {
   console.log("POST /apitechu/v2/movement");
   console.log(req.body);


   var httpClient = requestJson.createClient(baseMLabURL);  //inicializo mi url de conexión a la BD
   console.log("Alta de movimientos");
   var newMov ={
      "IBAN": req.body.IBAN,
       "Fecha": req.body.Fecha,
       "Concepto": req.body.Concepto,
       "Importe": req.body.Importe,
       "Saldo": req.body.Saldo
   }
  console.log (newMov);
  var httpClient = requestJson.createClient(baseMLabURL);
  httpClient.post ("movement?" + mLabAPIKey,newMov,
     function (err,resMLab, body){
       if (err){
          console.log(err);
          var response = {
             "msg" : "Error obteniendo movimientos"
          }
      }else{
       res.status(500);
     console.log("movimiento guardado");
     res.status(201).send({"msg": "movimiento creado"});
    }
    }
  )

  }
  function ingresoMov(req, res) {
    console.log("POST /apitechu/v2/ingreso");
    //dan un IBAN, un comentario y el Importe
    let now= hoyFecha();

    console.log('La fecha actual es',now);


    var id = req.body.IBAN;
    var query = 'q={"IBAN": "'+id + '"}';
    console.log("La consulta es:" + query);

    var httpClient = requestJson.createClient(baseMLabURL);  //inicializo mi url de conexión a la BD
    console.log("Busqueda de saldo del la cuenta");
    httpClient.get("account?"+ query + "&"+ mLabAPIKey,    //aqui le indico la colección y la apikey
     function(err,resMLab, myaccount){          //para asegurar lo que estoy preguntando puedo hacer un console del resMLab
        if (err){
           var response = {
              "msg" : "Error obteniendo dados de la cuenta"
         }
        res.status(500);
      }else{ // actualizo el saldo
          if (myaccount.length > 0){
             console.log("Saldo de la cuenta:" + myaccount[0].balance);
             myaccount[0].balance= myaccount[0].balance + req.body.Importe;
             console.log("Nuevo saldo de la cuenta:" + myaccount[0].balance);
             //query = 'q={"id" : ' + body[0].id +'}';
             var putBody = '{"$set":{"balance":' + myaccount[0].balance+'}}';

             httpClient.put("account?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
               function(errPUT, resMLabPUT, bodyPUT) {
                 console.log("PUT done");
                  if (err){
                     console.log(err);
                     var response = {
                        "msg" : "Error gravando movimientos"
                     }
                 }else {
                   var newMov ={
                      "IBAN": req.body.IBAN,
                       "Fecha": now,
                       "Concepto": req.body.Concepto,
                       "Importe": req.body.Importe,
                       "Saldo": myaccount[0].balance
                   }
                   console.log("nuevo movimiento:"+ newMov);
                   httpClient.post ("movement?" + mLabAPIKey,newMov,
                      function (err,resMLab, pasbody){
                        if (err){
                           console.log(err);
                           var response = {
                              "msg" : "Error grabando movimientos"
                           }
                       }else{

                       console.log("movimiento guardado");
    //                  res.status(200).send({"msg": "ingreso creado y guardado"});
                     }
                     }
                   )
                 }
               }
             )
             ///ahora actualizo los datos en movimientos

          }else{
            var response = {
            "msg" : "Cuenta no encontrada"
          }
          res.status(404);
        }
     }
    res.send(response);
  }
 )
  }

  function reintegroMov(req, res) {
    console.log("POST /apitechu/v2/reintegro");
    //dado un IBAN, un comentario y un Importe , resto del saldo de la cuenta
    let now= hoyFecha();

    var id = req.body.IBAN;
    var query = 'q={"IBAN": "'+id + '"}';
    console.log("La consulta es:" + query);

    var httpClient = requestJson.createClient(baseMLabURL);  //inicializo mi url de conexión a la BD
    console.log("Busqueda de saldo del la cuenta");
    httpClient.get("account?"+ query + "&"+ mLabAPIKey,    //aqui le indico la colección y la apikey
     function(err,resMLab, myaccount){          //para asegurar lo que estoy preguntando puedo hacer un console del resMLab
        if (err){
           var response = {
              "msg" : "Error obteniendo dados de la cuenta"
         }
        res.status(500);
      }else{ // actualizo el saldo
          if (myaccount.length > 0){
             console.log("Saldo de la cuenta:" + myaccount[0].balance);
             myaccount[0].balance= myaccount[0].balance - req.body.Importe;
             console.log("Nuevo saldo de la cuenta:" + myaccount[0].balance);
             var putBody = '{"$set":{"balance":' + myaccount[0].balance+'}}';

             httpClient.put("account?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
               function(errPUT, resMLabPUT, bodyPUT) {
                   if (err){
                     console.log(err);
                     var response = {
                        "msg" : "Error grabando movimientos"
                     }
                 }else {
                   var response = {
                      "msg" : "Reintegro registrado"
                   }
                   var newMov ={
                       "IBAN": req.body.IBAN,
                       "Fecha": now,
                       "Concepto": req.body.Concepto,
                       "Importe": req.body.Importe,
                       "Saldo": myaccount[0].balance
                   }

                   httpClient.post ("movement?" + mLabAPIKey,newMov,
                      function (err,resMLab, pasbody){
                        if (err){
                           console.log(err);
                           var response = {
                              "msg" : "Error grabando movimientos"
                           }
                       }
                     }
                   )
                 }
               }
             )
             ///ahora actualizo los datos en movimientos

          }else{
            var response = {
            "msg" : "Cuenta no encontrada"
          }
          res.status(404);
        }
     }
  res.send(response);
  }
  )
  }

  function transferMov(req, res) {
    console.log("POST /apitechu/v2/transfer");
    //dado un IBAN A y uno B, obtengo saldo de A , resto importe y envio a una cuenta B
    let now= hoyFecha();


    var to = {
      "IBAN": req.body.Destino,
      "Concepto": req.body.Concepto,
      "Importe": req.body.Importe,
    }

    var id = req.body.Origen;
    var query = 'q={"IBAN": "'+id + '"}';
    console.log("La consulta es:" + query);
    var httpClient = requestJson.createClient(baseMLabURL);  //inicializo mi url de conexión a la BD
    console.log("Busqueda de saldo del la cuenta");
    httpClient.get("account?"+ query + "&"+ mLabAPIKey,    //aqui le indico la colección y la apikey
     function(err,resMLab, myaccount){          //para asegurar lo que estoy preguntando puedo hacer un console del resMLab
        if (err){
           var response = {
              "msg" : "Error obteniendo dados de la cuenta"
         }
        res.status(500);
      }else{ // actualizo el saldo
          if (myaccount.length > 0){
             console.log("Saldo de la cuenta:" + myaccount[0].balance);
             myaccount[0].balance= myaccount[0].balance - req.body.Importe;
             console.log("Nuevo saldo de la cuenta:" + myaccount[0].balance);
             var putBody = '{"$set":{"balance":' + myaccount[0].balance+'}}';

             httpClient.put("account?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
               function(errPUT, resMLabPUT, bodyPUT) {
                   if (err){
                     console.log(err);
                     var response = {
                        "msg" : "Error grabando movimientos"
                     }
                 }else {
                   var response = {
                      "msg" : "Reintegro registrado"
                   }
                   var newMov ={
                       "IBAN": req.body.Origen,
                       "Fecha": now,
                       "Concepto": req.body.Concepto,
                       "Importe": req.body.Importe,
                       "Saldo": myaccount[0].balance
                   }

                   httpClient.post ("movement?" + mLabAPIKey,newMov,
                      function (err,resMLab, pasbody){
                        if (err){
                           console.log(err);
                           var response = {
                              "msg" : "Error grabando movimientos"
                           }
                       }
                     }
                   )
                   //envio a la otra cuenta
                 }
               }
             )
             ///ahora actualizo los datos en movimientos

          }else{
            var response = {
            "msg" : "Cuenta no encontrada"
          }
          res.status(404);
        }
     }
  res.send(response);
  }
  )


}
  function hoyFecha(){
      var hoy = new Date();
          var dd = hoy.getDate();
          var mm = hoy.getMonth()+1;
          var yyyy = hoy.getFullYear();

          dd = addZero(dd);
          mm = addZero(mm);

          return dd+'/'+mm+'/'+yyyy;
  }
  function addZero(i) {
    if (i < 10) {
        i = '0' + i;
    }
    return i;
}
module.exports.consultarMov = consultarMov;
module.exports.altaMov = altaMov;
module.exports.ingresoMov = ingresoMov;
module.exports.reintegroMov = reintegroMov;
module.exports.transferMov = transferMov;
