const requestJson = require('request-json');
const io= require('../io.js');
const crypt =require ('../crypt.js');

const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechumgf11ed/collections/"; //url base donde voy hacer busqueda, ojo con la barra
//const mLabAPIKey = "apiKey=5zTFmFoXB8txjvfwTLCXt6mCqPL-P_ea"; // hay que poner apikey=
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY; //recupero el valor de la variable de entorno


function loginUsersV1(req, res) {
  console.log("GET /apitechu/v1/login");

  var result = {};
  var users = require('../usuarios.json'); // .. que estoy en otra carpeta
  var encontrado = 0;

  for (var id=0;id<users.length;id++)
    {
      //  console.log("Proceso busqueda de email:" + users[id].email);
      if (req.body.email == users[id].email) {
            console.log("Email encontrado");

            if (req.body.password== users[id].password){
                res.send({"msg": "Login correcto, idUsuario :"+ users[id].id});
                encontrado =1;
                break;
            }else {
              console.log("Password no encontrado:");
    //          res.send({"msg": "Login incorrecto"});
            }
      }

    }
  // if (id== users.length){
  //   console.log("Email no encontrado");
  // }
 if (encontrado ==1){
      users[id].logged =true;
      console.log("Usuario a logear:" + users[id].logged);
   io.writeUserDataToFile(users);
 }
 else {
   res.send({"msg": "Login incorrecto"});
 }
 }
 // POST a /apitechu/v1/logout/:id
  // Id es la id del usuario a deslogar.
 // Tanto para logout correcto como incorrecto, devolver un mensaje informativo.
 //
 // En caso de logout correcto, guardar el cambio en el archivo.
 //
 // En el logout tenemos que quitar el campo logged del usuario (delete user.logged)
 //
 // Ejemplo de respuestas:
 //
 // Logout incorrecto { "mensaje" : "logout incorrecto" }
 //
 // Logout correcto { "mensaje" : "logout correcto", "idUsuario" : 1 }

 function logoutUsersV1(req, res) {
   console.log("GET /apitechu/v1/logout");

   var result = {};
   var users = require('../usuarios.json'); // .. que estoy en otra carpeta
   var id = req.params.id;
  id = id -1;
//  console.log("la id del usuario a logout es:" + req.params.id);
  console.log("la id del usuario a logout es:" + users[id].id);
  console.log("la id del usuario a logout es:" + users[id].logged);
  if (users[id].logged ==true) {
      console.log("usuario logado");
      delete users[id].logged ;
      console.log("Usuario a logear:" + users[id].logged);
//      users.push(users[id].logged);
      io.writeUserDataToFile(users);
      res.send({"msg": "Logout correcto idUsuario :"+ req.params.id});

      //       writeUserDataToFile(users);
    }  else{
      console.log("usuario no logado");
      res.send({"msg": "Logout incorrecto"});
    }
 }

 function loginUsersV2(req, res) {
   console.log("GET /apitechu/v2/loginUsersV2");

   var httpClient = requestJson.createClient(baseMLabURL);  //inicializo mi url de conexión a la BD

   var email = req.body.email;
   var pass = req.body.password;

   console.log ("email", email);
   console.log ("password", pass);

   var query = 'q={"email":"'+email + '"}';
   console.log("La consulta es:" + query);

   var putBody = '{"$set":{"logged":true}}';

   httpClient.get("user?"+ query + "&"+ mLabAPIKey,    //aqui le indico la colección y la apikey
    function(err,resMLab, body){
      if (err){
         var response = {
            "msg" : "Error obteniendo ususario a logar"
         }
      res.status(500);
    }else {
      if (body.length>0){
      console.log("usuario existe");
      console.log(body[0].passwords);
      if (crypt.checkPassword(pass,body[0].passwords)){
        console.log ("password correcta");
        httpClient.put ("user?" +query+ "&" + mLabAPIKey,JSON.parse(putBody),
           function (errPut,resMLab, bodyPut){
               console.log("pongo usuario como logado");
               var response = {
                  "msg" : "Usuario logado con exito"
               }
               res.send(response);
           }
        )
      }else{
        console.log("password incorrecta");
        var response = {
           "msg" : "usuario o password incorrecta"
        }
        res.status(404);
        res.send(response);
      }
    }else{
      console.log("usuario no encontrado");
      var response = {
         "msg" : "usuario o password incorrecta"
      }
      res.status(401);
      res.send(response);
    }
    }
    }
    )
}

function logoutUsersV2(req, res) {
  console.log("GET /apitechu/v2/logout");
  var id = req.params.id;
  var query = 'q={"id":'+id + '}'
  console.log("La consulta es:" + query);


  var httpClient = requestJson.createClient(baseMLabURL);

  httpClient.get("user?"+ query + "&"+ mLabAPIKey,
   function(err,resMLab, body){
     if (err){
        var response = {
           "msg" : "Error obteniendo ususario a logar"
        }
     }else {
     console.log("usuario existe");
     console.log("respuesta: " + body.length);

  //   if (body.length>0 && body[0].logged== true) {  //dos o una llamada
       if (body.length>0){
         if (body[0].logged){
           console.log("logged:",body[0].logged);
          var putBody = '{"$unset":{"logged":""}}';
          httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
            function (errPut, resMLab, bodyPut){
                if (err) {
                  var response =  {
                      "msg" : "Error actualizando logout del ususario"
                  }
                  console.log("Error actualizando logout del usuario");
                  res.send(response);
                }else{
                  var response =  {
                      "msg" : "Usuario desconectado"
                      }
                 console.log("OK usuario desconectado");
                 res.send(response);
                }
            }
          )
        }else {
          console.log("Usuario no logado");
           var response =  {
               "msg" : "Usuario no logado"
               }
            res.send(response);
        }
      }else{
        console.log("Usuario no encontrado");
         var response =  {
             "msg" : "Usuario no encontrado"
             }
          res.send(response);
      }
   }
 }
   )

}

module.exports.loginUsersV1 = loginUsersV1 ;
module.exports.loginUsersV2 = loginUsersV2 ;
module.exports.logoutUsersV1 = logoutUsersV1 ;
module.exports.logoutUsersV2 = logoutUsersV2 ;
