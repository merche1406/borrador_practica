const requestJson = require('request-json');
const io= require('../io.js');
const crypt =require ('../crypt.js');

const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechumgf11ed/collections/"; //url base donde voy hacer busqueda, ojo con la barra
//const mLabAPIKey = "apiKey=5zTFmFoXB8txjvfwTLCXt6mCqPL-P_ea"; // hay que poner apikey=
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY; //recupero el valor de la variable de entorno

function getUsersV2(req, res) {
 console.log("GET /apitechu/v2/users");

 var httpClient = requestJson.createClient(baseMLabURL);  //inicializo mi url de conexión a la BD
 console.log("Cliente creado");
 httpClient.get("user?"+ mLabAPIKey,    //aqui le indico la colección y la apikey
 function(err,resMLab, body){          //para asegurar lo que estoy preguntando puedo hacer un console del resMLab
   var response = !err ? body : {      // ! significa un not logico Si no hay error devuelvo el body , body[o] es 1
     "msg" : "Error obtenido usuario"
   }
   res.send(response);
 }
 )
}

function getUsersByIdV2(req, res) {
 console.log("GET /apitechu/v2/users/:id");

 var id = req.params.id;
 var query = 'q={"id":'+id + '}'
 console.log("La consulta es:" + query);

 var httpClient = requestJson.createClient(baseMLabURL);  //inicializo mi url de conexión a la BD
 console.log("Cliente busqueda");
 httpClient.get("user?"+ query + "&"+ mLabAPIKey,    //aqui le indico la colección y la apikey
  function(err,resMLab, body){          //para asegurar lo que estoy preguntando puedo hacer un console del resMLab
     if (err){
        var response = {
           "msg" : "Error obteniendo ususario"
      }
     res.status(500);
     }else{
       if (body.length > 0){
             var response = body[0];
       }else{
         var response = {
         "msg" : "Usuario no  encontrado"
       }
       res.status(404);
     }
  }
 res.send(response);
 }
// }
 )
}

function createUserV2(req,res){
  console.log("POST /apitechu/v2/users");
  console.log("id es:"+ req.body.id);
  console.log("first_name es:"+ req.body.first_name);
  console.log("last_name es:"+ req.body.last_name);
  console.log("email es:"+ req.body.email);
  console.log("password es:"+ req.body.password);

//busco el correo del usuario, antes de darlo de alta
 var query = 'q={"email": "' + req.body.email + '"}';
 console.log("La consulta del correo es " + query);

 var httpClient = requestJson.createClient(baseMLabURL);  //inicializo mi url de conexión a la BD
 console.log("Busqueda de cliente por email");
 httpClient.get("user?"+ query + "&"+ mLabAPIKey,    //aqui le indico la colección y la apikey
  function(err,resMLab, body){          //para asegurar lo que estoy preguntando puedo hacer un console del resMLab
     if (err){
        var response = {
           "msg" : "Error buscando ususario"
      }
     res.status(500);
   }else{
       if (body.length > 0){
            console.log("Correo encontrado:");
            console.log(body[0]);
            body[0].id= 0;
            var response = {
              "msg" : "Usuario existe, no se puede dar de alta",
              "idUsuario" : body[0].id
            }
             res.send(response);
       } else{

         var newUser ={
           "id" : req.body.id,
           "first_name" : req.body.first_name,
          "last_name ": req.body.last_name,
          "email": req.body.email,
          "password": crypt.hash(req.body.password)
         }
        console.log (newUser);
        var httpClient = requestJson.createClient(baseMLabURL);
        httpClient.post ("user?" + mLabAPIKey,newUser,
           function (err,resMLab, body){
           console.log("usuario guardado");
           res.status(201).send({"msg": "ususario creado"});
          }
        )
         }
      }
   }
)
 }

function deleteteUserV2(req,res){
  console.log("DELETE /apitechu/v2/users/:id");

  var id = req.params.id;
  var query = 'q={"id":' + id + '}';
  console.log("La consulta es " + query);

  var httpClient = requestJson.createClient(baseMLabURL);


  var bodyPut = [];

  httpClient.put("user?" + query + "&" + mLabAPIKey, bodyPut,
    function(err, resMLab, body) {
      res.send({"msg":"usuario borrado"});
    }
  )
}

function getUsersByEmail(req, res) {
 console.log("GET /apitechu/v2/busqueda/:email");

 var query = 'q={"email": "' + req.params.email + '"}';
 console.log("La consulta del correo es " + query);

 var httpClient = requestJson.createClient(baseMLabURL);  //inicializo mi url de conexión a la BD
 console.log("Busqueda de cliente por email");
 httpClient.get("user?"+ query + "&"+ mLabAPIKey,    //aqui le indico la colección y la apikey
  function(err,resMLab, body){          //para asegurar lo que estoy preguntando puedo hacer un console del resMLab
     if (err){
        var response = {
           "msg" : "Error buscando ususario"
      }
     res.status(500);
     }else{
       if (body.length > 0){
            console.log("Correo encontrado:");
            console.log(body[0]);
            var response = {
              "msg" : "Usuario existe",
              "idUsuario" : body[0].id
            }
       }else{
         var response = {
         "msg" : "Usuario no  encontrado"
       }
       res.status(404);
     }
  }
 res.send(response);
 }
// }
 )
}

module.exports.getUsersV2 = getUsersV2;
module.exports.getUsersByIdV2 = getUsersByIdV2;
module.exports.createUserV2 = createUserV2;
module.exports.deleteteUserV2 = deleteteUserV2;
module.exports.getUsersByEmail = getUsersByEmail;
