const requestJson = require('request-json');
const io= require('../io.js');
const crypt =require ('../crypt.js');

const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechumgf11ed/collections/"; //url base donde voy hacer busqueda, ojo con la barra
//const mLabAPIKey = "apiKey=5zTFmFoXB8txjvfwTLCXt6mCqPL-P_ea"; // hay que poner apikey=
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY; //recupero el valor de la variable de entorno

function getUsersV1(req, res) {
  console.log("GET /apitechu/v1/users");

  var result = {};
  var users = require('../usuarios.json'); // .. que estoy en otra carpeta

  if (req.query.$count == "true") {
    console.log("Count needed");
    result.count = users.length;
  }

  result.users = req.query.$top ?
    users.slice(0, req.query.$top) : users;

  res.send(result);
}

function getUsersV2(req, res) {
 console.log("GET /apitechu/v2/users");

 var httpClient = requestJson.createClient(baseMLabURL);  //inicializo mi url de conexión a la BD
 console.log("Cliente creado");
 httpClient.get("user?"+ mLabAPIKey,    //aqui le indico la colección y la apikey
 function(err,resMLab, body){          //para asegurar lo que estoy preguntando puedo hacer un console del resMLab
   var response = !err ? body : {      // ! significa un not logico Si no hay error devuelvo el body , body[o] es 1
     "msg" : "Error obtenido usuario"
   }
   res.send(response);
 }
 )
}

function getUsersByIdV2(req, res) {
 console.log("GET /apitechu/v2/users/:id");

 var id = req.params.id;
 var query = 'q={"id":'+id + '}'
 console.log("La consulta es:" + query);

 var httpClient = requestJson.createClient(baseMLabURL);  //inicializo mi url de conexión a la BD
 console.log("Cliente busqueda");
 httpClient.get("user?"+ query + "&"+ mLabAPIKey,    //aqui le indico la colección y la apikey
  function(err,resMLab, body){          //para asegurar lo que estoy preguntando puedo hacer un console del resMLab
     if (err){
        var response = {
           "msg" : "Error obteniendo ususario"
      }
     res.status(500);
     }else{
       if (body.length > 0){
             var response = body[0];
       }else{
         var response = {
         "msg" : "Usuario no  encontrado"
       }
       res.status(404);
     }
  }
 res.send(response);
 }
// }
 )
}

function createUserV2(req,res){
  console.log("POST /apitechu/v2/users");
  console.log("id es:"+ req.body.id);
  console.log("first_name es:"+ req.body.first_name);
  console.log("last_name es:"+ req.body.last_name);
  console.log("email es:"+ req.body.email);
  console.log("password es:"+ req.body.password);

 var newUser ={
   "id" : req.body.id,
   "first_name" : req.body.first_name,
   "last_name ": req.body.last_name,
   "email": req.body.email,
   "passwords": crypt.hash(req.body.password)
 }
 console.log (newUser);
 var httpClient = requestJson.createClient(baseMLabURL);
 httpClient.post ("user?" + mLabAPIKey,newUser,
    function (err,resMLab, body){
      console.log("usuario guardado");
      res.status(201).send({"msg": "ususario creado"});
    }
)
}

function deleteteUserV2(req,res){
 console.log("DELETE /apitechu/v2/users/:id");

 var id = req.params.id;
 var query = 'q={"id":'+id + '}' //ojo, pregunto por un numero , si es un string tengo que poner comillas
 console.log("La consulta es:" + query);
 var delUser =[];

 var httpClient = requestJson.createClient(baseMLabURL);
 // httpClient.put("user?" + query + "&" + mLabAPIKey, delUser,
 //    function (err, resMLab, body){
 //      if(err){
 //        var response = {
 //          "msg" : "Error borando usuario"
 //        }
 //        res.status(500);
 //      } else {
 //        var response = {
 //          "msg" : "Usuario borrado " + id
 //        }
 //      }
 //      res.send(response);
 //    }
 //    )
 httpClient.get("user?"+ query + "&"+ mLabAPIKey,    //aqui le indico la colección y la apikey
  function(err,resMLab, body){
    if (err){
       var response = {
          "msg" : "Error obteniendo ususario a borrar"
       }
    res.status(500);
    }else{
      if (body.length > 0){
        httpClient.put ("user?" +query+ "&" + mLabAPIKey,delUser,
           function (err,resMLab, body){
             if (err){
                console.log ("Error del borrado" + err);
                var response ={
                    "msg": "Error en el proceso de borrado del Usuario: " ,
                    "id ": id
                 }
             }else{
                console.log("Usuario borrado");
                var response ={
                    "msg": "Usuario borrado" ,
                    "id_ususario": id
                 }
                 res.send(response);
             }
           }
        )
      }else{
        var response = {
        "msg" : "Usuario no  encontrado"
        }
       res.status(404);
      }
   }
  console.log("que tiene el response" + response);
res.send(response);
 }
 )
}

function createUserV1(req,res){
  console.log("POST /apitechu/v1/users");

//  console.log(req.body);  me muestra la informacion del body    console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);

  var newUser = {
    "first_name" : req.body.first_name,
    "last_name" : req.body.last_name,
    "email" : req.body.email
  };

  var users = require ('../usuarios.json');
  users.push(newUser); //meto un elemento al alert-primary
  console.log("usuario añadido al array");

  io.writeUserDataToFile(users);
  console.log("Proceso de creacion de usuario terminado");

  res.send({"msg": "usuario creado"});


}
function deleteUserV1(req,res){
  console.log("DELETE /apitechu/v1/users/:id");

  console.log("la id del usuario a borrar es:" + req.params.id);
  var users = require('../usuarios.json');
//  users.splice(req.params.id -1,1);

// for (var id=0;id<users.length;id++)
//   {
//     console.log("por donde voy:" + id);
//     if (req.params.id ==id){
//       users.splice(req.params.id -1,1);
//       writeUserDataToFile(users);
//       console.log("Proceso de borrado de usuario terminado");
//
//       res.send({"msg": "usuario borrado"});
//
//     }


//  arr.forEach(function callback(currentValue, index, array) {
    // tu iterador

//    var array1 = ['a', 'b', 'c'];
//     var id =0;
//     users.forEach(function element(id, ) {
//       console.log(element.id);
//       if (req.params.id ==element.id)
//              users.splice(req.params.id -1,1);
//              writeUserDataToFile(users);
//              console.log("Proceso de borrado de usuario terminado");
//              res.send({"msg": "usuario borrado"});
//       }
//        i++;
//     });
//   }
// )
//

////for tipo IN

for (const prop in users) {
console.log(`users.${prop} `)  ;
if (req.params.id == prop) {
       users.splice(req.params.id -1,1);
       io.writeUserDataToFile(users);
       console.log("Proceso de borrado de usuario terminado");
       res.send({"msg": "usuario borrado"});
       break;
}

}
}

function getAccountById(req, res) {
 console.log("GET /apitechu/v2/account/:id");

 var id = req.params.id;
 var query = 'q={"id":'+id + '}'
 console.log("La consulta es:" + query);

 var httpClient = requestJson.createClient(baseMLabURL);  //inicializo mi url de conexión a la BD
 console.log("Cliente busqueda");
 httpClient.get("user?"+ query + "&"+ mLabAPIKey,    //aqui le indico la colección y la apikey
  function(err,resMLab, body){          //para asegurar lo que estoy preguntando puedo hacer un console del resMLab
     if (err){
        var response = {
           "msg" : "Error obteniendo ususario"
      }
     res.status(500);
     }else{
       if (body.length > 0){
             var response = body[0];
       }else{
         var response = {
         "msg" : "Usuario no  encontrado"
       }
       res.status(404);
     }
  }
 res.send(response);
 }
// }
 )
}

module.exports.getUsersV1 = getUsersV1;
module.exports.getUsersV2 = getUsersV2;
module.exports.getUsersByIdV2 = getUsersByIdV2;
module.exports.createUserV1 = createUserV1;
module.exports.createUserV2 = createUserV2;
module.exports.deleteUserV1 = deleteUserV1;
module.exports.deleteteUserV2 = deleteteUserV2;
module.exports.getAccountById= getAccountById;
