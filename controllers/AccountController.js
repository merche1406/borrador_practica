const requestJson = require('request-json');
const io= require('../io.js');
const crypt =require ('../crypt.js');

const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechumgf11ed/collections/"; //url base donde voy hacer busqueda, ojo con la barra
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY; //recupero el valor de la variable de entorno

function getAccountById(req, res) {
 console.log("GET /apitechu/v2/account/:id");

 var id = req.params.id;
 var query = 'q={"userid":'+id + '}'
 console.log("La consulta es:" + query);

 var httpClient = requestJson.createClient(baseMLabURL);  //inicializo mi url de conexión a la BD
 console.log("Cliente busqueda");
 httpClient.get("account?"+ query + "&"+ mLabAPIKey,    //aqui le indico la colección y la apikey
  function(err,resMLab, body){          //para asegurar lo que estoy preguntando puedo hacer un console del resMLab
     if (err){
        var response = {
           "msg" : "Error obteniendo ususario"
      }
     res.status(500);
     }else{
       if (body.length > 0){
  //           var response = body[0];
           var response = body;
       }else{
         var response = {
         "msg" : "Usuario no  encontrado"
       }
       res.status(404);
     }
  }
 res.send(response);
 }
// }
 )
}

function createAccount(req, res) {
 console.log("POST /apitechu/v2/account/id");
 console.log("userid es:"+ req.params.id);

var id =req.params.id;


 var newAccount ={
   "userid": + id ,
//   "IBAN" : "ES84 3333 6153 3ZQQ RJJ8 RSUT HA2S MEE",
    "IBAN" : generaIBAN(),
   "balance": 0
 }
 console.log (newAccount);
 var httpClient = requestJson.createClient(baseMLabURL);  //inicializo mi url de conexión a la BD
 console.log("Crear cuenta a un cliente");
 httpClient.post ("account?" + mLabAPIKey,newAccount,
    function (err,resMLab, body){
      if(err){
         var response = {
            "msg" : "Error obteniendo ususario"
       }
      res.status(500);
      console.log(err);
    }else{
    console.log("cuenta creada");
    res.status(201).send({"msg": "Cuenta de cliente creada"});
    }
   }
 )

}

function deleteAccount(req, res) {
 console.log("DELETE /apitechu/v2/account/:id");

 var cuenta = req.params.id;
 var query = 'q={"IBAN": "'+ cuenta + '"}';
 console.log("La consulta es:" + query);
 var delaccount = [];

 var httpClient = requestJson.createClient(baseMLabURL);
  httpClient.put("account?" + query + "&" + mLabAPIKey, delaccount,
  function(err,resMLab, body){
     if (err){
        var response = {
           "msg" : "Error elminando la cuenta"
      }
     res.status(500);
   }else {
     var response = {
        "msg" : "Cuenta eliminada correctamente"
      }
    }
 res.satus(200).send(response);
 }
 )
}



function generaIBAN() {

    var newiban =""
    for ( var i = 0; i < 10; i++) {
        newiban += Math.floor(Math.random() * 10);
    }

    return 'ES84' + '0182' + newiban;

};
module.exports.getAccountById = getAccountById;
module.exports.createAccount = createAccount;
module.exports.deleteAccount = deleteAccount;
