const requestJson = require('request-json');
const io= require('../io.js');
const crypt =require ('../crypt.js');

const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechumgf11ed/collections/"; //url base donde voy hacer busqueda, ojo con la barra
//const mLabAPIKey = "apiKey=5zTFmFoXB8txjvfwTLCXt6mCqPL-P_ea"; // hay que poner apikey=
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY; //recupero el valor de la variable de entorno

function getUsersV1(req, res) {
  console.log("GET /apitechu/v1/users");

  var result = {};
  var users = require('../usuarios.json'); // .. que estoy en otra carpeta

  if (req.query.$count == "true") {
    console.log("Count needed");
    result.count = users.length;
  }

  result.users = req.query.$top ?
    users.slice(0, req.query.$top) : users;

  res.send(result);
}


function createUserV1(req,res){
  console.log("POST /apitechu/v1/users");

//  console.log(req.body);  me muestra la informacion del body    console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);

  var newUser = {
    "first_name" : req.body.first_name,
    "last_name" : req.body.last_name,
    "email" : req.body.email
  };

  var users = require ('../usuarios.json');
  users.push(newUser); //meto un elemento al alert-primary
  console.log("usuario añadido al array");

  io.writeUserDataToFile(users);
  console.log("Proceso de creacion de usuario terminado");

  res.send({"msg": "usuario creado"});


}
function deleteUserV1(req,res){
  console.log("DELETE /apitechu/v1/users/:id");

  console.log("la id del usuario a borrar es:" + req.params.id);
  var users = require('../usuarios.json');
//  users.splice(req.params.id -1,1);

// for (var id=0;id<users.length;id++)
//   {
//     console.log("por donde voy:" + id);
//     if (req.params.id ==id){
//       users.splice(req.params.id -1,1);
//       writeUserDataToFile(users);
//       console.log("Proceso de borrado de usuario terminado");
//
//       res.send({"msg": "usuario borrado"});
//
//     }


//  arr.forEach(function callback(currentValue, index, array) {
    // tu iterador

//    var array1 = ['a', 'b', 'c'];
//     var id =0;
//     users.forEach(function element(id, ) {
//       console.log(element.id);
//       if (req.params.id ==element.id)
//              users.splice(req.params.id -1,1);
//              writeUserDataToFile(users);
//              console.log("Proceso de borrado de usuario terminado");
//              res.send({"msg": "usuario borrado"});
//       }
//        i++;
//     });
//   }
// )
//

////for tipo IN

for (const prop in users) {
console.log(`users.${prop} `)  ;
if (req.params.id == prop) {
       users.splice(req.params.id -1,1);
       io.writeUserDataToFile(users);
       console.log("Proceso de borrado de usuario terminado");
       res.send({"msg": "usuario borrado"});
       break;
}

}
}

function getAccountById(req, res) {
 console.log("GET /apitechu/v2/account/:id");

 var id = req.params.id;
 var query = 'q={"userid":'+id + '}'
 console.log("La consulta es:" + query);

 var httpClient = requestJson.createClient(baseMLabURL);  //inicializo mi url de conexión a la BD
 console.log("Cliente busqueda");
 httpClient.get("account?"+ query + "&"+ mLabAPIKey,    //aqui le indico la colección y la apikey
  function(err,resMLab, body){          //para asegurar lo que estoy preguntando puedo hacer un console del resMLab
     if (err){
        var response = {
           "msg" : "Error obteniendo ususario"
      }
     res.status(500);
     }else{
       if (body.length > 0){
  //           var response = body[0];
           var response = body;
       }else{
         var response = {
         "msg" : "Usuario no  encontrado"
       }
       res.status(404);
     }
  }
 res.send(response);
 }
// }
 )
}


module.exports.getUsersV1 = getUsersV1;

module.exports.createUserV1 = createUserV1;

module.exports.deleteUserV1 = deleteUserV1;
module.exports.getAccountById = getAccountById;
