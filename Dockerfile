# Dockerfile

#Imagen raiz
FROM node

#Carpeta raiz
WORKDIR /apitechu

#Copia de archivos de local a imagen. El punto es donde estoy lanzando el bluid(donde tengo el código) y /apitechu es donde deja la imagen
ADD . /apitechu

#Instalación de las dependencias. --only=prod es que solo me traigo las de producción y no las de desarrollo , ni los test
RUN npm install --only=prod

#Puerto que vamos a usar
EXPOSE 3000

#comando de inicializacion
CMD ["node", "server.js"]
