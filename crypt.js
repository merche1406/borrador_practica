const bcrypt = require('bcrypt');

// funcion de hash de password
function hash(data){
  console.log("hasing data");
  return bcrypt.hashSync(data, 10); //devuelve un String
}

// funcion que comprueba uan password
function checkPassword(passwordFromUserInPlainText,passwordFromDBHashed){
  console.log("Checking password");

 // retorna un boolean
  return bcrypt.compareSync(passwordFromUserInPlainText,passwordFromDBHashed);
}

module.exports.hash = hash;
module,exports.checkPassword = checkPassword;
